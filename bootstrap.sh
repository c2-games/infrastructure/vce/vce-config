#!/bin/sh

bw config server $BW_SERVER
bw login --apikey --quiet
bw unlock --passwordenv BW_MASTER --quiet
export BW_SESSION=$(bw unlock --passwordenv BW_MASTER --raw)

export KC_FEDERATION_CLIENT_SECRET=$(bw get password --organizationid a98b067e-d781-4747-b41a-6863138f3840 a36c4559-4fcf-46e3-8132-c1a24761c958)
export GQL_ENDPOINT=$(bw get item a36c4559-4fcf-46e3-8132-c1a24761c958 | jq -r '.fields[] | select(.name == "GQL_ENDPOINT") | .value')
# echo $KC_FEDERATION_CLIENT_SECRET - $GQL_ENDPOINT

pbs_username=$(bw get item 5fe64278-f4c3-4938-8610-5ec5dff6d08c | jq -r '.login | .username')
pbs_password=$(bw get password --organizationid a98b067e-d781-4747-b41a-6863138f3840 5fe64278-f4c3-4938-8610-5ec5dff6d08c)
pbs_host=$(bw get item 5fe64278-f4c3-4938-8610-5ec5dff6d08c | jq -r '.login | .uris[0] | .uri')
bw get item 5fe64278-f4c3-4938-8610-5ec5dff6d08c | jq -r '.fields[] | select(.name == "key") | .value' > /root/pbs.key

# echo $pbs_username - $pbs_password - $pbs_host && cat /root/pbs.key

export PBS_REPOSITORY="${pbs_username}@${pbs_host}:sandbox"
export PBS_PASSWORD=$pbs_password

pvesm add pbs pbs-vce --datastore sandbox --server $pbs_host --namespace vce \
        --username $pbs_username --password $pbs_password --encryption-key /root/pbs.key

KC_CLIENT=$(bw get password --organizationid a98b067e-d781-4747-b41a-6863138f3840 77288818-1056-452d-b9fb-0424091be3f0)
pveum realm modify prod --client-key $KC_CLIENT

proxmox-backup-client restore --ns vce --keyfile /root/pbs.key host/sandbox2 scripts.pxar /root/scripts

# Add cloudflare tunnel
cf_token=$(bw get item 43b50ffc-2fc4-4ee8-b612-709fba7b352a | jq -r ".fields[] | select(.name == \"$HOSTNAME\") | .value")
curl -L --output cloudflared.deb https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb
dpkg -i cloudflared.deb
cloudflared service install $cf_token
systemctl status cloudflared

# Add staff and student accounts to PVE
/root/scripts/staff-pve-accounts.sh &
/root/scripts/blueteam-pve-accounts-local.py &

# Schedule VCE actions for the day - competition open, lunch lockdown, etc.
/root/scripts/schedule.py

# Transfer images from PBS server
bash /root/scripts/restore/restore-all.sh

# Build Red team kali, scoring workers, and blue team templates
bash /root/scripts/restore/build-scoring.sh &
bash /root/scripts/restore/red.kali.sh &
bash /root/scripts/restore/blue.sh 

# Restore Cody's VMs and create his taps
bash /root/scripts/restore/corelight.sh

# Configure test scoring team
bash /root/scripts/restore/configure-team0.sh
