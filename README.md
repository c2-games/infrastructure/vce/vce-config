# Competition Server Deployment & Configuration Outline

Stages:
1. [vce-deploy](https://gitlab.com/c2-games/infrastructure/vce/vce-deploy) - Provision the competition servers
2. [vce-config](https://gitlab.com/c2-games/infrastructure/vce/vce-config) (you are here) - Provide initial configuration and bootstrap of a competition server
3. [vce-onboarding](https://gitlab.com/c2-games/infrastructure/vce/vce-onboarding) - Add NCAE content for the competition and create team environments


# Stage 2: VCE Server Configuration

## Apply required environment variables:

* export BW_SERVER=
* export BW_CLIENTID=
* export BW_CLIENTSECRET=
* export BW_MASTER=

These are vaulted at `vault?collectionId=ebe6884a-81a3-4f33-9d5d-3197eb0c3f86&itemId=9687b519-3bf1-4c98-ab05-f520fff87c72`.


# VCE Configuration public repo items:
 * `proxmox` - nginx configuration downloaded by the VCE-Deploy repo's proxmox-deploy script
 * `rules.v4` - IPTables NAT rules downloaded by the VCE-Deploy repo's proxmox-deploy script
 * `interfaces` - Additional competition interfaces; to be appended to default interfaces file 
 * `bootstrap.sh` - Second stage script to download VMs/scripts and begin configuring a new competition server

The proxmox-deploy script will pull and apply the first three files and download the bootstrap script to `/root/`.
The bootstrap is the only component here to be manually executed after applying the vars above.

**WARNING:** These files will be automatically downloaded by proxmox-deploy from the **main** branch.
